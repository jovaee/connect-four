package Enum;

/**
 * @author Johann van Eeden
 */
public enum Seed {

    EMPTY,
    RED,
    RED_SHADOW,
    YELLOW,
    YELLOW_SHADOW
}
