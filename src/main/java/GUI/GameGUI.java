package GUI;

import Enum.Seed;
import Logic.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Line2D;
import java.awt.geom.RoundRectangle2D;

/**
 * @author Johann van Eeden
 */
public class GameGUI extends JFrame {

    private int NUM_COLS = 7;
    private int NUM_ROWS = 6;

    private final double SIDE_PADDING = 5;

    // Predefined initial cell sizes. Updates when window size changes
    private double CELL_HEIGHT = 100;
    private double CELL_WIDTH = 100;

    // Predefined initial window size. Updates when window size changes
    private int WINDOW_HEIGHT = (int) Math.ceil(CELL_HEIGHT * (NUM_ROWS + 1) + SIDE_PADDING * 2);
    private int WINDOW_WIDTH = (int) Math.ceil(CELL_WIDTH * NUM_COLS + SIDE_PADDING * 2);

    private final Color COLOR_RED_PLAYER = new Color(255, 0, 0);
    private final Color COLOR_RED_PLAYER_SHADOW = new Color(255, 0, 0, 135);
    private final Color COLOR_YELLOW_PLAYER = new Color(255, 255, 0);
    private final Color COLOR_YELLOW_PLAYER_SHADOW = new Color(255, 255, 0, 135);

    private int SHADOW_LAST_COLUMN = -1;

    private double DIFFICULTY = 0.6;

    private Seed[][] board;

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {}

        EventQueue.invokeLater(() -> new GameGUI());
    }

    /**
     * Constructor
     */
    public GameGUI() {
        board = Logic.initBoard(NUM_ROWS, NUM_COLS);

        init();

        showStartupPopup();
    }

    private void showStartupPopup() {
        StartupPopup sp = new StartupPopup();
        sp.setVisible(true);
    }

    /**
     * Cleans the logical board and the GUI.GameGUI board
     */
    private void restartGame() {
        board = Logic.initBoard(NUM_ROWS, NUM_COLS);

        repaint();
    }

    /**
     * Calculate the size each cell should be according to the current window size
     */
    private void calcCellSizes() {
        CELL_WIDTH = (WINDOW_WIDTH - 2 * SIDE_PADDING) / (double) NUM_COLS;
        CELL_HEIGHT = (WINDOW_HEIGHT - 2 * SIDE_PADDING) / (double) (NUM_ROWS + 1);
    }

    /**
     *
     */
    private void init() {
        setTitle("Connect-Four");
        setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        MenuItem mi1 = new MenuItem("Very Easy");
        mi1.addActionListener(e -> {
            DIFFICULTY = 0.2;

            restartGame();
        });

        MenuItem mi2 = new MenuItem("Easy");
        mi2.addActionListener(e -> {
            DIFFICULTY = 0.4;

            restartGame();
        });

        MenuItem mi3 = new MenuItem("Normal");
        mi3.addActionListener(e -> {
            DIFFICULTY = 0.6;

            restartGame();
        });

        MenuItem mi4 = new MenuItem("Hard");
        mi4.addActionListener(e -> {
            DIFFICULTY = 0.8;

            restartGame();
        });

        MenuItem mi5 = new MenuItem("Very Hard");
        mi5.addActionListener(e -> {
            DIFFICULTY = 1.0;

            restartGame();
        });

        PopupMenu pm = new PopupMenu("Difficulty Setting");
        pm.add(mi1);
        pm.add(mi2);
        pm.add(mi3);
        pm.add(mi4);
        pm.add(mi5);

        DrawCanvas canvas = new DrawCanvas();
        canvas.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                super.mouseMoved(e);

                int col = (int) ((e.getX() - 2 * SIDE_PADDING) / CELL_WIDTH);

                if (SHADOW_LAST_COLUMN == -1 || SHADOW_LAST_COLUMN != col) {
//                    System.out.println(col);

                    SHADOW_LAST_COLUMN = col;

                    repaint();
                }
            }
        });
        canvas.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);

                if (e.getButton() == MouseEvent.BUTTON3) {
                    pm.show(canvas, e.getX(), e.getY());
                } else if (e.getButton() == MouseEvent.BUTTON1) {
                    int tmp;
//                    int row = (int) ((e.getY() - 2 * SIDE_PADDING) / CELL_HEIGHT);
                    int col = (int) ((e.getX() - 2 * SIDE_PADDING) / CELL_WIDTH);

//                System.out.printf("Row: %d\tCol: %d\n", row, col);

                    // User player move
                    tmp = Logic.dropTile(board, Seed.YELLOW, NUM_ROWS, col);

                    if (tmp == -1) {
                        return;
                    } else if (Logic.isWin(board, NUM_ROWS, NUM_COLS, tmp, col)) {
                        repaint();

                        JOptionPane.showMessageDialog(null, "Player YELLOW has won");
                        restartGame();

                        return;
                    }

                    repaint();

                    // CPU player move
                    State apple = Solver.solve(board, DIFFICULTY);

                    if (apple == null) {
                        JOptionPane.showMessageDialog(null, "NFGJSDHFKJSDBHF DGHFUIYSDF&&D^F&DTFI*");
                    } else {
                        apple = Solver.getFirstState(apple);

                        board = apple.board;
                        repaint();

                        if (Logic.isWin(board, NUM_ROWS, NUM_COLS, apple.rowAdded, apple.colAdded)) {
                            JOptionPane.showMessageDialog(null, "Player RED has won");
                            restartGame();

                            return;
                        }
                    }
                }
            }
        });
        canvas.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);

                WINDOW_WIDTH = e.getComponent().getWidth();
                WINDOW_HEIGHT = e.getComponent().getHeight();

                calcCellSizes();
                repaint();
            }
        });

        canvas.add(pm);

        add(canvas);

        setVisible(true);
        setLocationRelativeTo(null);
    }

    private class DrawCanvas extends JPanel {

        @Override
        public void paint(Graphics g) {
            super.paint(g);

            Graphics2D g2 = (Graphics2D) g;

            setBackground(Color.WHITE);

            g2.setStroke(new BasicStroke(1.5f));

            // Draw horizontal borders
            for (int i = 0; i < NUM_ROWS + 1; i = i + 1) {
                g2.draw(new Line2D.Double(SIDE_PADDING, i * CELL_HEIGHT + SIDE_PADDING + CELL_HEIGHT, WINDOW_WIDTH - SIDE_PADDING, i * CELL_HEIGHT + SIDE_PADDING + CELL_HEIGHT));
            }

            // Draw vertical borders
            for (int i = 0; i < NUM_COLS + 1; i = i + 1) {
                g2.draw(new Line2D.Double(i * CELL_WIDTH + SIDE_PADDING, SIDE_PADDING + CELL_HEIGHT, i * CELL_WIDTH + SIDE_PADDING, WINDOW_HEIGHT - SIDE_PADDING));
            }

            // Draw seeds
            g2.setStroke(new BasicStroke(1.0f));

            g2.setColor(COLOR_YELLOW_PLAYER_SHADOW);
            g2.fill(new RoundRectangle2D.Double((SHADOW_LAST_COLUMN * CELL_WIDTH + CELL_WIDTH * 0.05) + SIDE_PADDING, CELL_HEIGHT * 0.05 + SIDE_PADDING, CELL_WIDTH * 0.9, CELL_HEIGHT * 0.9, 150, 150));


            for (int i = 0; i < NUM_ROWS; i = i + 1) {
                for (int j = 0; j < NUM_COLS; j = j + 1) {
                    if (board[i][j] == Seed.RED) {
                        g2.setColor(COLOR_RED_PLAYER);
                    } else if (board[i][j] == Seed.YELLOW) {
                        g2.setColor(COLOR_YELLOW_PLAYER);
                    }

                    if (board[i][j] != Seed.EMPTY) {
                        g2.fill(new RoundRectangle2D.Double((j * CELL_WIDTH + CELL_WIDTH * 0.05) + SIDE_PADDING, i * CELL_HEIGHT + CELL_HEIGHT * 0.05 + CELL_HEIGHT + SIDE_PADDING, CELL_WIDTH * 0.9, CELL_HEIGHT * 0.9, 150, 150));
                    }
                }
            }
        }
    }
}
