package GUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;

import net.miginfocom.swing.MigLayout;

/**
 * @author Johann van Eeden
 */
public class StartupPopup extends JDialog {

    /**
     * Create the dialog.
     */
    public StartupPopup() {
        setResizable(false);
        setModal(true);
        setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
        setModalityType(ModalityType.APPLICATION_MODAL);
        setTitle("How-To-Play");
        setBackground(Color.WHITE);
        getContentPane().setBackground(Color.WHITE);
        setBounds(100, 100, 902, 900);		{

            getContentPane().setLayout(new MigLayout("", "[396px,grow][grow]", "[][grow][408px,grow][][33px]"));
            {
                JLabel lblNewLabel = new JLabel("To win connect FOUR YELLOW circles as indicated below");
                lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
                getContentPane().add(lblNewLabel, "cell 0 0 2 1,alignx center,growy");
            }
        }
        {
            JLabel lblNewLabel_1 = new JLabel("");
            lblNewLabel_1.setIcon(new ImageIcon(this.getClass().getResource("/straight_1.png")));
            getContentPane().add(lblNewLabel_1, "cell 0 1,alignx center");
        }
        {
            JLabel label = new JLabel("");
            label.setIcon(new ImageIcon(this.getClass().getResource("/straight_2.png")));
            getContentPane().add(label, "cell 1 1,alignx center");
        }
        {
            JLabel label = new JLabel("");
            label.setIcon(new ImageIcon(this.getClass().getResource("/diag_1.png")));
            getContentPane().add(label, "cell 0 2,alignx center");
        }
        {
            JLabel label = new JLabel("");
            label.setIcon(new ImageIcon(this.getClass().getResource("/diag_2.png")));
            getContentPane().add(label, "cell 1 2,alignx center");
        }
        {
            JLabel lblNewLabel_2 = new JLabel("Right clicking will bring up a menu where the CPU difficulty can be changed");
            lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 12));
            getContentPane().add(lblNewLabel_2, "cell 0 3 2 1,grow");
        }
        {
            JButton okButton = new JButton("OK");
            okButton.addActionListener(arg0 -> dispose());
            getContentPane().add(okButton, "cell 0 4 2 1,grow");
            okButton.setActionCommand("OK");
            getRootPane().setDefaultButton(okButton);
        }
    }
}
