package Logic;

import Enum.Seed;

public class State {

    public final State parent;

    public final Seed lastMover;
    public final Seed[][] board;

    public  int rowAdded;
    public  int colAdded;
    public final int moves;

    public State(State parent, Seed lastMover, Seed[][] board, int rowAdded, int colAdded, int moves) {
        this.parent = parent;

        this.lastMover = lastMover;
        this.board = board;

        this.rowAdded = rowAdded;
        this.colAdded = colAdded;
        this.moves = moves;
    }
}
