package Logic;

import Enum.Seed;

/**
 *
 */
public class Logic {

    /**
     * Create an empty board with all cells initialized as <i>EMPTY</i>
     */
    public static Seed[][] initBoard(int numRows, int numCols) {
        Seed[][] board = new Seed[numRows][numCols];

        for (int i = 0; i < numRows; i = i + 1) {
            for (int j = 0; j < numCols; j = j + 1) {
                board[i][j] = Seed.EMPTY;
            }
        }

        return board;
    }

    /**
     *
     *
     * @param board
     * @param seed The player dropping the tile
     * @param numRows
     *@param col The column the tile should be dropped in  @return The row the tile was placed in. -1 is returned f the tile could not be placed
     */
    public static int dropTile(Seed[][] board, Seed seed, int numRows, int col) {
        int tmpRow = 0;

        if (board[tmpRow][col] != Seed.EMPTY) {
//            System.err.println("The column is already full");

            return -1;
        }

        for (int i = 1; i < numRows; i = i + 1) {
            if (board[i][col] == Seed.EMPTY) {
                tmpRow = i;
            } else {
                break;
            }
        }

        board[tmpRow][col] = seed;

        return tmpRow;
    }

    /**
     * Check if the player with the tile at <i>row</i> and <i>col</i> is a winner
     * @param row The row to start checking from
     * @param col The column to start checking from
     * @return True is it is a win otherwise False
     */
    public static boolean isWin(Seed[][] board, int numRows, int numCols, int row, int col) {
        if (row < 0 || col < 0) {
            return false;
        }

        Seed seed = board[row][col];

        int count;

        // Look in row
        count = 0;
        for (int i = 0; i < numRows; i = i + 1) {
            if (board[i][col] == seed) {
                count += 1;
            } else {
                count = 0;
            }

            if (count == 4) {
                return true;
            }
        }

        // Look in col
        count = 0;
        for (int i = 0; i < numCols; i = i + 1) {
            if (board[row][i] == seed) {
                count += 1;
            } else {
                count = 0;
            }

            if (count == 4) {
                return true;
            }
        }

        // Look left-to-right down
        count = 0;
        for (int i = row, j = col; i < numRows && j < numCols; i = i + 1, j = j + 1) {
            if (board[i][j] == seed) {
                count += 1;
            } else {
                count = 0;
            }

            if (count == 4) {
                return true;
            }
        }

        // Look left-to-right up
        count = 0;
        for (int i = row, j = col; i >= 0 && j < numCols; i = i - 1, j = j + 1) {
            if (board[i][j] == seed) {
                count += 1;
            } else {
                count = 0;
            }

            if (count == 4) {
                return true;
            }
        }

        // Look right-to-left down
        count = 0;
        for (int i = row, j = col; i < numRows && j >= 0; i = i + 1, j = j - 1) {
            if (board[i][j] == seed) {
                count += 1;
            } else {
                count = 0;
            }

            if (count == 4) {
                return true;
            }
        }

        // Look right-to-left up
        count = 0;
        for (int i = row, j = col; i >= 0 && j >= 0; i = i - 1, j = j - 1) {
            if (count == 4) {
                return true;
            } else if (board[i][j] == seed) {
                count += 1;
            } else {
                count = 0;
            }
        }

        return false;
    }

}
