package Logic;

import java.util.*;

import Enum.Seed;

/**
 * @author Johann van Eeden
 */
public class Solver {

    private static Queue<State> todo = new LinkedList<State>();

    private static State bestRedWin = null;
    private static State bestYellowWin = null;

    /**
     *
     * @param board
     * @param diff
     * @return
     */
    public static State solve(Seed[][] board, double diff) {
        todo.clear();

        bestRedWin = null;
        bestYellowWin = null;

        if (!randomMoveMade(board, diff)) {
            todo.add(new State(null, Seed.YELLOW, board, 0, -1, -1));

//            System.out.println("---------------------------");
            while (!todo.isEmpty()) {

                State tmp = todo.poll();

                if (Logic.isWin(tmp.board, tmp.board.length, tmp.board[0].length, tmp.rowAdded, tmp.colAdded)) {
                    if (tmp.lastMover == Seed.RED && bestRedWin == null) {
                        bestRedWin = tmp;
//                        System.err.println("First bestRedWin");
                        continue;
                    } else if (tmp.lastMover == Seed.RED && bestRedWin.moves > tmp.moves) {
                        bestRedWin = tmp;
//                        System.err.println("New bestRedWin");
                        continue;
                    } else if (tmp.lastMover == Seed.YELLOW && bestYellowWin == null) {
                        bestYellowWin = tmp;
//                        System.err.println("First bestYellowWin");
                        continue;
                    } else if (tmp.lastMover == Seed.YELLOW && bestYellowWin.moves > tmp.moves) {
                        bestYellowWin = tmp;
//                        System.err.println("New bestYellowWin");
                        continue;
                    }
                }

                dropAllTiles(tmp);
            }
        }


//        System.err.println(bestRedWin.moves);
//        System.err.println(bestYellowWin.moves);

        if (bestYellowWin == null && bestRedWin == null) {
//            System.err.println("RETARD");
            return null;
        } else if (bestYellowWin == null) {
//            System.err.println("YELLOW NULL");
            return bestRedWin;
        } else if (bestRedWin == null) {
//            System.err.println("RED NULL");
            return bestYellowWin;
        } else if (bestYellowWin.moves == 1) {
//            System.err.println("YELLOW LESS");
            return bestYellowWin;
        } else {
            return bestRedWin;
        }
    }

    /**
     *
     * @param board
     * @param diff
     * @return
     */
    private static boolean randomMoveMade(Seed[][] board, double diff) {
        int row;
        int col;

        Random r = new Random();

        if (1.0 - diff >= r.nextDouble()) {

            do {
                col = r.nextInt(board[0].length);
            } while ((row = Logic.dropTile(board, Seed.RED, board.length, col)) == -1);

            bestRedWin = new State(null, Seed.RED, board, row, col, 0);

            return true;
        }

        return false;
    }

    /**
     *
     * @param state
     * @return
     */
    public static State getFirstState(State state) {

        if (state.lastMover == Seed.RED) {
            while (state.moves != 0) {
                state = state.parent;
            }

            return state;
        } else {
            while (state.moves != 1) {
                state = state.parent;
            }

            state.parent.board[state.rowAdded][state.colAdded] = Seed.RED;
            state.parent.board[state.parent.rowAdded][state.parent.colAdded] = Seed.EMPTY;

            state.parent.rowAdded = state.rowAdded;
            state.parent.colAdded = state.colAdded;

            return state.parent;
        }
    }

    /**
     *
     * @param board
     * @return
     */
    private static Seed[][] copyBoard(Seed[][] board) {
        Seed[][] newBoard = new Seed[board.length][board[0].length];

        for (int i = 0; i < board.length; i = i + 1) {
            for (int j = 0; j < board[0].length; j = j + 1) {
                newBoard[i][j] = board[i][j];
            }
        }

        return newBoard;
    }

    /**
     *
     * @param state
     */
    private static void dropAllTiles(State state) {

        if (state.lastMover == Seed.RED && bestRedWin != null && bestRedWin.moves <= state.moves) {
            return;
        } else if (bestYellowWin != null && bestYellowWin.moves <= state.moves) {
            return;
        }

        Seed[][] currentBoard = state.board;
        Seed[][] tmp;

        int rowDropped;

        for (int i = 0; i < currentBoard[0].length; i = i + 1) {
            tmp = copyBoard(currentBoard);

            if (state.lastMover == Seed.RED) {
                rowDropped = Logic.dropTile(tmp, Seed.YELLOW, currentBoard.length, i);
                if (rowDropped >= 0) {
                    todo.add(new State(state, Seed.YELLOW, tmp, rowDropped, i, state.moves + 1));
                }
            } else {
                rowDropped = Logic.dropTile(tmp, Seed.RED, currentBoard.length, i);
                if (rowDropped >= 0) {
                    todo.add(new State(state, Seed.RED, tmp, rowDropped, i, state.moves + 1));
                }
            }
        }
    }
}
